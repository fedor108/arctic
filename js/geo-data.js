if (!arcticPanoramaData) {
	var arcticPanoramaData = {};
}

// Текст для соцсетей
arcticPanoramaData.share = {
	title: "Панорамы русской Арктики",
	description: "Добро пожаловать на самую северную часть России! Теперь у вас есть возможность увидеть её своими глазами",
	url: ""
};

// Текущая позиция панорамы (и точка входа приложения)
var tempCurrent = {
	xml: "ARCTICNEQ4-20130730/0001.xml",
	island: "гукера",
	hlookat: 43,
	vlookat: -3
};

if (window["localStorage"] && JSON) {
	var savedLastPanorama = JSON.parse(window.localStorage.getItem("ArcticLastPanorama"));
	if (savedLastPanorama) {
		arcticPanoramaData.current = savedLastPanorama;
	} else {
		arcticPanoramaData.current = tempCurrent;
	}
} else {
	arcticPanoramaData.current = tempCurrent;
}
// console.log(arcticPanoramaData.current);

// Какие панорамы относятся к каким островам (желаемые точки входа по клику на карту/список)
arcticPanoramaData.islands = {
	"гукера": { xml: "ARCTICNEQ4-20130730/0001.xml", hlookat: 43, vlookat: -3 },
	"нортбрук": { xml: "ARCTICNEQ4-20130805/0002.xml", hlookat: 287, vlookat: -14 },
	"луиджи": { xml: "ARCTICNEQ4-20130806/0001.xml", hlookat: 233, vlookat: -11 },
	"куна": { xml: "ARCTICNEQ4-20130808/0001.xml", hlookat: 13, vlookat: -6 },
	"столичка": { xml: "ARCTICNEQ4-20130809/0001.xml", hlookat: 182, vlookat: -9 },
	"виннер-нойштадт": { xml: "ARCTICNEQ4-20130810/0002.xml", hlookat: 205, vlookat: 1 },
	"галля": { xml: "ARCTICNEQ4-20130811/0001.xml", hlookat: 195, vlookat: -4 },
	"хейса": { xml: "ARCTICNEQ4-20130812/0002.xml", hlookat: 162, vlookat: -1 },
	"алджер": { xml: "ARCTICNEQ4-20130814/0006.xml", hlookat: 252, vlookat: -1 },
	"рудольфа": { xml: "ARCTICNEQ4-20130819/0001.xml", hlookat: 234, vlookat: -1 },
	"белл": { xml: "ARCTICNEQ4-20130825/0001.xml", hlookat: 176, vlookat: -12 }
};

// Какие директори к каким островам относятся
arcticPanoramaData.folders = {
	"ARCTICNEQ4-20130730": "гукера",
	"ARCTICNEQ4-20130731": "гукера",
	"ARCTICNEQ4-20130804": "гукера",
	"ARCTICNEQ4-20130805": "нортбрук",
	"ARCTICNEQ4-20130806": "луиджи",
	"ARCTICNEQ4-20130808": "куна",
	"ARCTICNEQ4-20130809": "столичка",
	"ARCTICNEQ4-20130810": "виннер-нойштадт",
	"ARCTICNEQ4-20130811": "галля",
	"ARCTICNEQ4-20130812": "хейса",
	"ARCTICNEQ4-20130813": "хейса",
	"ARCTICNEQ4-20130814": "алджер",
	"ARCTICNEQ4-20130819": "рудольфа",
	"ARCTICNEQ4-20130821": "белл",
	"ARCTICNEQ4-20130825": "белл"
};

// Достопримечательности
arcticPanoramaData.interesting = [
	{
		/* о. Гукера */
		title: "Научно-исследовательская станция",
		xml: "ARCTICNEQ4-20130730/0001.xml",
		image: "images/place-gukera-30-0001.jpg",

		hlookat: -5,
		vlookat: -2
	},
	{
		/* о. Гукера */
		title: "Крест экспедиции Седова",
		xml: "ARCTICNEQ4-20130730/0038.xml",
		image: "images/place-gukera-30-0038.jpg",
		hlookat: 95,
		vlookat: -9
	},
	{
		/* о. Галля */
		title: "Мыс Тегетхофф",
		xml: "ARCTICNEQ4-20130811/0015.xml",
		image: "images/place-gallja-11-0015.jpg",
		hlookat: -57,
		vlookat: -11
	},
	{
		/* о. Хейса */
		title: "Самое северное почтовое отделение в мире",
		xml: "ARCTICNEQ4-20130813/0002.xml",
		image: "images/place-hejsa-13-0002.jpg",
		hlookat: 140,
		vlookat: -3
	},
	{
		/* о. Хейса */
		title: "Обломки самолета ИЛ-14",
		xml: "ARCTICNEQ4-20130812/0032.xml",
		image: "images/place-hejsa-12-0032.jpg",
		hlookat: 235,
		vlookat: -4
	},
	{
		/* о. Хейса */
		title: "Указатель расстояний до разных городов мира",
		xml: "ARCTICNEQ4-20130813/0011.xml",
		image: "images/place-hejsa-13-0011.jpg",
		hlookat: -33,
		vlookat: -9
	},
	{
		/* о. Нортбрук */
		title: "Крест Альбанова",
		xml: "ARCTICNEQ4-20130805/0016.xml",
		image: "images/place-nortbruk-05-0016.jpg",
		hlookat: -11,
		vlookat: -15
	},
	{
		/* о. Алджер */
		title: "Станция полярного исследователя Ирвина Болдуина",
		xml: "ARCTICNEQ4-20130814/0006.xml",
		image: "images/place-aljer-14-0006.jpg",
		hlookat: -57,
		vlookat: 7
	},
	{
		/* о. Рудольфа */
		title: "Мыс Флигели – Поклонный крест памяти Георгия Седова",
		xml: "ARCTICNEQ4-20130819/0007.xml",
		image: "images/place-rudolfa-19-0007.jpg",
		hlookat: 243,
		vlookat: -5
	},
	{
		/* о. Белл */
		title: "Дом Эйры",
		xml: "ARCTICNEQ4-20130825/0005.xml",
		image: "images/place-bell-25-0005.jpg",
		hlookat: 243,
		vlookat: -5
	}
];


// Слайдшоу
arcticPanoramaData.slideshow = {
	points: [
		{ xml: "ARCTICNEQ4-20130825/0005.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130819/0003.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130819/0001.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130819/0007.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130819/0009.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130814/0006.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130813/0014.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130813/0011.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130813/0007.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130813/0002.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130812/0002.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130812/0021.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130812/0032.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130812/0041.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130811/0008.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130811/0014.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130811/0015.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130810/0006.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130810/0012.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130808/0009.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130806/0010.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130805/0003.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130805/0016.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130805/0020.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130730/0001.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130730/0037.xml", hlookat: 1, vlookat: 1 },
		{ xml: "ARCTICNEQ4-20130730/0045.xml", hlookat: 1, vlookat: 1 }
	],
	current: {
		id: 0
	}
};

// Всё подряд по порядку
arcticPanoramaData.all = {
	info: {
		"ARCTICNEQ4-20130730": { length: 53, exceptions: [41] },
		"ARCTICNEQ4-20130731": { length: 2 },
		"ARCTICNEQ4-20130804": { length: 2 },
		"ARCTICNEQ4-20130805": { length: 26, exceptions: [1] },
		"ARCTICNEQ4-20130806": { length: 10 },
		"ARCTICNEQ4-20130808": { length: 9 },
		"ARCTICNEQ4-20130809": { length: 11 },
		"ARCTICNEQ4-20130810": { length: 15, exceptions: [1] },
		"ARCTICNEQ4-20130811": { length: 15 },
		"ARCTICNEQ4-20130812": { length: 54, exceptions: [1] },
		"ARCTICNEQ4-20130813": { length: 19 },
		"ARCTICNEQ4-20130814": { length: 8, exceptions: [1] },
		"ARCTICNEQ4-20130819": { length: 10 },
		"ARCTICNEQ4-20130821": { length: 9 },
		"ARCTICNEQ4-20130825": { length: 5 }
	},
	order: [
		"ARCTICNEQ4-20130730",
		"ARCTICNEQ4-20130731",
		"ARCTICNEQ4-20130804",
		"ARCTICNEQ4-20130805",
		"ARCTICNEQ4-20130806",
		"ARCTICNEQ4-20130808",
		"ARCTICNEQ4-20130809",
		"ARCTICNEQ4-20130810",
		"ARCTICNEQ4-20130811",
		"ARCTICNEQ4-20130812",
		"ARCTICNEQ4-20130813",
		"ARCTICNEQ4-20130814",
		"ARCTICNEQ4-20130819",
		"ARCTICNEQ4-20130821",
		"ARCTICNEQ4-20130825",
	]
};
