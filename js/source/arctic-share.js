
//
// Шаринг
//

angular.module("Arctic")
	.directive("arcticShare", ["$rootScope", "ArcticData", "$location", function ($rootScope, ArcticData, $location) {
		return {
			restrict: "EA",
			link: function ($scope, $element, $attrs) {

				//
				// Формируем ссылку
				//

				$rootScope.$on("$locationChangeStart", function (event, url) {
					var host = url.split("#/")[0],
						path = url.split("#/")[1] ? url.split("#/")[1].split("?")[1] : "";
					ArcticData.share.url = host + "#/panorama?" + path;
					ArcticData.share.urlencode = encodeURIComponent(ArcticData.share.url);
					ArcticData.share.image = encodeURIComponent(host + "/images/share.jpg");
				});

				//
				// Инициализируем содержание шаринга
				//

				$scope.share = ArcticData.share;

				//
				// Сокращаем ссылку через goo.gl API
				// (каждый раз когда открывается окно шаринга)
				//

				$rootScope.$on("stateChangeSuccess", function (event, data) {
					if (data.state == "share") {
						setTimeout(function () {
							$.ajax({
								"url":"https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyCILUJqpKnGZzQSyDFmSjpn5RnKoPOgMsY",
								"method": "POST",
								"contentType": "application/json",
								"data": JSON.stringify({ longUrl: $scope.share.url }),
								"processData": false
							})
							.done(function (response) {
								if (response.id) {
									$scope.share.shorturl = response.id;
									$rootScope.$apply();
								}
							});
						}, 10);
					}
				});

				//
				// Автоматическое выделение текста
				//

				$element.find(".url").on("focus", function (event, data) {
					$(this).select();
					event.preventDefault();
				});

				$element.find(".url").on("click", function (event, data) {
					// $(this).select();
					event.preventDefault();
					return false;
				});

				//
				// Делимся
				//

				$scope.shareFB = function () {
					// console.log("FB", ArcticData.share);
					FB.ui({
					  method: 'feed',
					  link: ArcticData.share.url,
					  caption: ArcticData.share.title,
					  description: ArcticData.share.description
					}, function (response) {
						// console.log(response);
					});
				};

				$scope.shareTW = function () {
					// console.log("TW", ArcticData.share);
				};

				$scope.shareVK = function () {
					// console.log("VK", ArcticData.share);
				};

				$scope.shareGP = function () {
					// console.log("GP", ArcticData.share);
				};
			}
		}
	}])