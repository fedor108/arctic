
//
// UI-скрипты, которые проще без angular
// (в т.ч. хаки)
//

$(document).on("ready", function () {

	// Саморесайзящиеся текстарии
	autosize($("textarea"));
	setTimeout(function () {
		autosize.update($("textarea"));
	},100)

	// Хак прокрутки всех "страниц" page вверх при переходах "страница"
	// Чтоб срабатывал отключение подсветки кнопки меню и закрытия
	angular.element(document.documentElement).scope().$on("stateChangeSuccess", function (event, data) {
		$("page").scrollTop(0);
		setTimeout(function () {
			autosize.update($("textarea"));
		},100)
	});

});

$(window).on("load", function () {
	// Саморесайзящиеся текстарии
	setTimeout(function () {
		autosize.update($("textarea"));
	},100);

	// Прелоадер
	setTimeout(function () {
		$("arctic-app").removeAttr("hidden");
		$("preloader").hide();
	},1);
});

// Супермегахак для знания какой броузер прямо в стилях
// через html[user-agent*="MSIE 9"] например
$("html").attr("user-agent", navigator.userAgent);