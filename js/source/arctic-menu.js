
//
// Меню
//

angular.module("Arctic")
	.directive("arcticMenu", ["$rootScope", "$timeout", "$location", "ArcticData", "Panorama", function ($rootScope, $timeout, $location, ArcticData, Panorama) {

		function toggleMenu ($scope, force) {
			$scope.menu.opened = (force == true ? true : (force == false ? false : !$scope.menu.opened))
			if ($scope.menu.opened) {
				$rootScope.$broadcast("menu-opened");
			} else {
				$rootScope.$broadcast("menu-closed");
			}
		}

		return {
			restrict: "EA",
			link: function ($scope, $element, $attr) {

				$scope.menu = {};

				//
				// Вкл/выкл меню в мобильной версии по кнопке <toggle>
				//

				$element.on("click", "toggle", function () {
					toggleMenu($scope);
					$scope.$apply();
				});

				//
				// При клике на элементах меню, закрываем его (на мобильных)
				//

				$element.on("click", "li", function (event) {
					toggleMenu($scope, false);
					$scope.$apply();
				});

				//
				// При переходах куда-либо тоже закрываем
				//

				$rootScope.$on("stateChangeSuccess", function (event, data) {
					toggleMenu($scope, false);
				});

				//
				// Слайдшоу
				//

				$scope.slideshow = {
					active: false,
					resume: function () {
						var slideshow = this;
						if ($rootScope.state.name != "panorama") {
							$rootScope.state.go("panorama");
						}
						// Перемешать порядок точек
						slideshow.shuffle(ArcticData.slideshow.points);
						// Запускаем
						Panorama.api.then(function (krpano) {
							slideshow.active = true;
							$rootScope.slideshowActive = true;
							ArcticData.slideshow.current.id = 0;
							var currentPanoramaInfo = ArcticData.slideshow.points[ArcticData.slideshow.current.id];
							// Если текущая панорама не текущая по списку, подставляем текущую по списку
							if (ArcticData.current.xml != currentPanoramaInfo.xml) {
								Panorama.load({
									xml: currentPanoramaInfo.xml,
									hlookat: currentPanoramaInfo.hlookat,
									vlookat: currentPanoramaInfo.vlookat,
									autorotate: true
								});
							} else {
								var h = parseInt(krpano.get("view.hlookat"));
								if (isNaN(h)) {
									h = 0;
								}
								while (h > 360) {
									h = h - 360;
								}
								while (h < 0) {
									h = h + 360;
								}
								Panorama.setNumberVar("view.hlookat", h, krpano);
								// krpano.set("view.hlookat", h);
							}
							// крутим
							krpano.set("autorotate.enabled", true);
							krpano.set("autorotate.speed", 10);

							$rootScope.fullscreen($("arctic-app").get(0));

							$rootScope.$broadcast("slideshowStatusChange", true);
						});
					},
					pause: function () {
						var slideshow = this;
						Panorama.api.then(function (krpano) {
							krpano.set("autorotate.enabled", false);
							$rootScope.slideshowActive = false;
							if (slideshow.active == true) {
								$rootScope.exitFullscreen();
							}
							slideshow.active = false;
							$rootScope.$broadcast("slideshowStatusChange", false);
						});
					},
					toggle: function () {
						if (this.active) {
							this.pause();
						} else {
							this.resume();
						}
					},
					next: function () {
						if (ArcticData.slideshow.current.id + 1 < ArcticData.slideshow.points.length) {
							ArcticData.slideshow.current.id += 1;
						} else {
							ArcticData.slideshow.current.id = 0;
						}

						var currentPanoramaInfo = ArcticData.slideshow.points[ArcticData.slideshow.current.id];
						Panorama.load({
							xml: currentPanoramaInfo.xml,
							hlookat: currentPanoramaInfo.hlookat,
							vlookat: currentPanoramaInfo.vlookat,
							autorotate: true
						});
					},
					shuffle: function (points) {
						var temp = [];
						while (points.length > 0) {
							var randomId = Math.floor(Math.random()*points.length);
							temp.push(points[randomId]);
							points.splice(randomId, 1);
						}
						while (temp.length > 0) {
							points.push(temp.shift());
						}
					}
				};

				// При повороте на 360 градусов, сменяем слайд
				var wait = false;
				$rootScope.$on("viewChangeSuccess", function (event, data) {
					var hlookatDelta = data.hlookat - ArcticData.slideshow.points[ArcticData.slideshow.current.id].hlookat;
					// console.log("viewchange", data.hlookat, hlookatDelta);
					if (hlookatDelta > 360 && $scope.slideshow.active && !wait) {
						$scope.slideshow.next();
						wait = true;
					}
				});
				$rootScope.$on("panoramaChangeSuccess", function (event, data) {
					wait = false;
				});

				// Нужно прекращать слайдшоу при действиях пользователя
				$rootScope.$on("stateChangeSuccess", function (event, data) {
					if ((data.state == "home" || data.state == "places" || data.state == "map") && $scope.slideshow.active) {
						$scope.slideshow.pause();
					}
				});
				$rootScope.$on("nextPanorama", function (event, data) {
					$scope.slideshow.pause();
				});
				$rootScope.$on("prevPanorama", function (event, data) {
					$scope.slideshow.pause();
				});
				$rootScope.$on("myloadpano", function (event, data) {
					$scope.slideshow.pause();
				});
				$rootScope.$on("panoramauseraction", function (event, data) {
					$scope.slideshow.pause();
				});
			}
		};
	}]);