//
// Роутинг страниц
//

angular.module("Arctic")
	.run(["$rootScope", "$location", function ($rootScope, $location) {

		// Simple routing
		var stateList = {
			"home": {
				name: "home",
				url: "/"
			},
			"about": {
				name: "about",
				url: "/about"
			},
			"panorama": {
				name: "panorama",
				url: "/panorama"
			},
			"island": {
				name: "island",
				url: "/island"
			},
			"share": {
				name: "share",
				url: "/share"
			},
			"places": {
				name: "places",
				url: "/places"
			},
			"map": {
				name: "map",
				url: "/map"
			},
			"slideshow": {
				name: "slideshow",
				url: "/slideshow"
			}
		};

		var journal = JSON.parse(localStorage.getItem("ArcticJournal"));

		$rootScope.state = {
			name: "home",
			url: "/",
			started: (localStorage.getItem("ArcticJournal") ? true : false),
			go: function (state, params) {
				if (this.name != stateList[state].name) {
					this.name = stateList[state].name;
					$rootScope.$broadcast("stateChangeSuccess", {
						state: state,
						params: params
					});
				}
				$location.path(stateList[state].url);
				if (params) {
					$location.search(params);
				}
				if (state == "panorama" && !this.started) {
					var self = this;
					setTimeout(function () {
						self.started = true;
						$rootScope.$apply();
					}, 500);
					// console.log("%cПутешествие началось","color: #257cbe; font-size: 120%;")
				}
			},
			toggle: function (state, params) {
				if (state != this.name) {
					this.go(state, params);
				} else {
					this.go("panorama", params);
				}
			}
		};


		$rootScope.$on("$locationChangeStart", function () {
			var path = $location.path(),
				params = $location.search();

			// Роутинг непонятных ссылок
			if (!path || path == "/" || path.split("/").length < 2) {
				$rootScope.state.go("home");
			} else {
				$rootScope.state.go(path.split("/")[1], params);
			}
		});

		// При загрузках панорамы, апдейтить location
		$rootScope.$on("panoramaChangeSuccess", function (event, data) {
			var params = {
				xml: data.xml
			};
			$location.search(angular.extend($location.search(), params));
		});

		// При изменениях вида, апдейтить location
		$rootScope.$on("viewChangeSuccess", function (event, data) {
			// console.log('data: ', data);
			var params = {
				hlookat: (parseInt(parseFloat(data.hlookat) * 1000) / 1000),
				vlookat: (parseInt(parseFloat(data.vlookat) * 1000) / 1000)
			};
			// console.log('params: ', params, data.hlookat, data.vlookat);
			$location.search(angular.extend($location.search(), params));
		});

		// При изменениях location (вручную переделываем ссылки)
		// надо бы передать инфу в панораму. А может и не стоит
		// $rootScope.$on("$locationChangeStart", function () {
		// 	var params = $location.search();
		// 	console.log(params);
		// });
	}]);