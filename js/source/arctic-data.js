
//
// Данные панорамы в html
//

angular.module("Arctic")
	.value("ArcticData", arcticPanoramaData)
	.run(["$rootScope", "ArcticData", function ($rootScope, ArcticData) {
		// Для удобства выносим в scope данные
		if (!ArcticData) {
			// console.log("Нет данных о панорамах - панорамы не будут работать.")
		}
		$rootScope.arcticData = ArcticData;
	}]);