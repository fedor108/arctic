
//
// Достаём SVG
//

function extractSVG (images, callback) {
	images.each(function () {
		var image = $(this);
		$.get(image.attr("src"), function (contents) {
			var svg = $(contents).find("svg");
			svg.attr("class", image.attr("class"));
			svg.attr("id", image.attr("id"));
			image.after(svg);
			image.remove();
			callback(svg);
		});
	});
}

$(document).on("ready", function () {
	$("img[extract-svg]").each(function () {
		extractSVG($(this));
	});
});