
//
// Основная директива приложения
//

angular.module("Arctic")
	.directive("arcticApp", ["$rootScope", "$location", "$timeout", function ($rootScope, $location, $timeout) {
		return {
			restrict: "EA",
			link: function ($scope, $element, $attr) {

				//
				// Слушаем события меню (нужно затенятся)
				//

				$rootScope.$on("menu-opened", function () {
					$element.addClass("menu-opened");
				});

				$rootScope.$on("menu-closed", function () {
					$element.removeClass("menu-opened");
				});

				//
				// Фулскрин
				//

				// Find the right method, call on correct element
				$rootScope.fullscreen = function (element) {
					if(element.requestFullscreen) {
						element.requestFullscreen();
					} else if(element.mozRequestFullScreen) {
						element.mozRequestFullScreen();
					} else if(element.webkitRequestFullscreen) {
						element.webkitRequestFullscreen();
					} else if(element.msRequestFullscreen) {
						element.msRequestFullscreen();
					}
				}

				$rootScope.exitFullscreen = function () {
					if(document.exitFullscreen) {
						document.exitFullscreen();
					} else if(document.mozCancelFullScreen) {
						document.mozCancelFullScreen();
					} else if(document.webkitExitFullscreen) {
						document.webkitExitFullscreen();
					}
				}

				$rootScope.fullscreenPanorama = function () {
					$rootScope.fullscreen(document.getElementById("krpanoSWFObject"));
				};

				//
				// При изменении панорамы, сохраняем её
				//

				function saveLastPanorama () {
					if (window["localStorage"]) {
						window.localStorage.setItem("ArcticLastPanorama", angular.toJson($rootScope.arcticData.current));
					}
				}

				$rootScope.$on("panoramaChangeSuccess", function () {
					saveLastPanorama();
				});

				var delayedViewTimeout;

				function delayedViewUpdate () {
					clearTimeout(delayedViewTimeout);
					delayedViewTimeout = setTimeout(function () {
						saveLastPanorama();
						$rootScope.$apply();
					}, 1000);
				}

				$rootScope.$on("viewChangeSuccess", function () {
					delayedViewUpdate();
				});
			}
		};
	}])