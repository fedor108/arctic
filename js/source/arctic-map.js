
//
// Карта (список + море с островами)
//

angular.module("Arctic")

	.service("ExtractSVG", function () {
		return extractSVG;
	})

	.directive("arcticMap", ["$rootScope", "$timeout", "ExtractSVG", "ArcticData", "Panorama", "$location", function ($rootScope, $timeout, ExtractSVG, ArcticData, Panorama, $location) {

		function setCurrentIslandOnMap(options) {
			options.mapView.find("svg").each(function () {
				var svg = $(this);
				svg.find("g[id]").attr("class", "");
				svg.find("#"+options.island).attr("class", "active");
			});

			options.listView.find("li[data-id]").removeClass("active");
			options.listView.find("li[data-id='" + options.island + "']").addClass("active");
		}


		return {
			restrict: "EA",
			link: function ($scope, $element, $attr) {

				var mapView = $element.find(".map-view"),
					listView = $element.find(".list-view");

				//
				// Преобразуем картинку в svg (инициализация карты)
				// и ставим якорь на начальный остров
				//

				ExtractSVG(mapView.find("img"), function () {
					// console.log(ArcticData.current.island);
					setCurrentIslandOnMap({
						mapView: mapView,
						listView: listView,
						island: ArcticData.current.island
					});
				});

				//
				// Слушаем клики по карте
				//

				mapView.on("click", "svg g[id]", function (event) {
					var group = $(this),
						id = group.attr("id"),
						island = ArcticData.islands[id];
					Panorama.load({
						xml: island.xml,
						hlookat: (island.hlookat ? island.hlookat : 1),
						vlookat: (island.vlookat ? island.vlookat : 1)
					});
					$rootScope.state.go("panorama");
					// $rootScope.$apply();
				});

				//
				// Слушаем клики по списку островов
				//

				listView.on("click", "li[data-id]", function (event) {
					var island = ArcticData.islands[$(this).attr("data-id")];
					Panorama.load({
						xml: island.xml,
						hlookat: (island.hlookat ? island.hlookat : 1),
						vlookat: (island.vlookat ? island.vlookat : 1)
					});
					$rootScope.state.go("panorama");
				})

				//
				// Слушаем события переключения панорамы, чтоб
				// поменять текущий остров
				//

				$rootScope.$on("panoramaChangeSuccess", function (event, data) {
					setCurrentIslandOnMap({
						mapView: mapView,
						listView: listView,
						island: data.island
					});
				});

			}
		};
	}]);