
//
// Прогресс-бар
//

angular.module("Arctic")
	.directive("arcticProgress", ["$rootScope", function ($rootScope) {
		var journal = {};

		if (window["localStorage"]) {
			var savedJournalString = window.localStorage.getItem("ArcticJournal");
			if (savedJournalString) {
				journal = angular.fromJson(savedJournalString);
			}
		}

		// Каждый раз при переходе панорамы (но не внутри слайдшоу)
		// помечать её посещенной

		var slideshowActive = false;

		$rootScope.$on("slideshowStatusChange", function (event, status) {
			slideshowActive = status;
		});

		return {
			restrict: "EA",
			template: "<span class='bar' ng-style='{ width: progessPercent }'></span>",
			link: function ($scope, $element, $attr) {

				$scope.progress = {};
				// $scope.progessPercent = "0%";

				//
				// Для прогресса надо знать
				// 1. общее кол-во слайдов на острове
				// 2. кол-во уже посещенного
				//

				$rootScope.$on("panoramaChangeSuccess", function (event, data) {
					if (!slideshowActive) {
						var island = $rootScope.arcticData.folders[data.xml.split("/")[0]];
						// у нас есть остров, нужно посчитать кол-во слайдов в каждой директории
						// относящейся к этому острову
						var total = 0;
						for (var i in $rootScope.arcticData.folders) {
							if ($rootScope.arcticData.folders[i] == island) {
								total += $rootScope.arcticData.all.info[i].length - ($rootScope.arcticData.all.info[i].exceptions ? $rootScope.arcticData.all.info[i].exceptions.length : 0);
							}
						}
						// нужно записать текущий слайд в журнал
						journal[island] = journal[island] || { visited : [] };
						if (journal[island].visited.indexOf(data.xml) < 0 ) {
							journal[island].visited.push(data.xml);
							if (window["localStorage"]) {
								// console.log(angular.toJson(journal));
								window.localStorage.setItem("ArcticJournal", angular.toJson(journal));
							}
						}
						angular.extend($scope.progress, {
							island: island,
							total: total,
							absolute: journal[island].visited.length,
							relative: (journal[island].visited.length / total)*100 + "%"
						});
						$scope.progessPercent = (journal[island].visited.length / total)*100 + "%";
						setTimeout(function () {
							// $scope.$apply();
						})
						// console.log($scope.progress, $scope.progessPercent);
					}
				});
			}
		};
	}])