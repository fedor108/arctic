//
// Основной модуль
//

angular.module("Arctic", ["ngAnimate"]);

// Вручную запускаем Angular
$(document).on("ready", function () {
	angular.bootstrap(document.documentElement, ["Arctic"]);
	// insertPanorama();
});