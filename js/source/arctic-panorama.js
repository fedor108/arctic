

//
// У нас есть внешние функции, которые вызываются из
// krpano-панорамы. Мы их пересылаем внутрь angular
//

// Загрузка панорамы по клику на стрелочки внутри панорамы
function myloadpano (xml, options) {
	angular.element(document.documentElement).scope().$broadcast("myloadpano", {
		xml: xml,
		options: options
	});
}

// Каждый раз при смене вида (поворот, глубина fov)
function updhash() {
	angular.element(document.documentElement).scope().$broadcast("updhash", {
		view: document.getElementById("krpanoSWFObject").get("view")
	});
}

// При готовности api krpano
function onkrpanoready (krpano) {
	setTimeout(function () {
		angular.element(document.documentElement).scope().$broadcast("krpanoready", {
			krpano: krpano
		});
	}, 100);
}

// При любом взаимодействии пользователя с панорамой (click, mousewheel)
function onpanoramauseraction () {
	angular.element(document.documentElement).scope().$broadcast("panoramauseraction");
}

//
// Панорама krpano
//

angular.module("Arctic")
	.factory("Embedpano", function () {
		return embedpano;
	})

	.value("TourPath", "tour/")

	.factory("Panorama", ["$rootScope", "Embedpano", "$q", "ArcticData", "TourPath", "$timeout", function ($rootScope, Embedpano, $q, ArcticData, TourPath, $timeout) {

		var deferredKrpanoApi = $q.defer();

		//
		// Слушаем внешние события от krpano
		//

		$rootScope.$on("myloadpano", function (event, data) {
			// Панорама загружена, осталось сделать внутренние angular-вещи:
			// проставить ссылку, оповестить кого надо
			// console.log("myloadpano", data);
			loadPanorama(data);
			$timeout(function () {
				$rootScope.$apply();
			},1);
		});

		$rootScope.$on("updhash", function (event, data) {
			// Вид сменился внутри панорамы, апдейтим только если на >= 1 градус изменилось
			// console.log(data);
			if (Math.abs(ArcticData.current.hlookat - data.view.hlookat) >= 1) {
				angular.extend(ArcticData.current, {
					hlookat: data.view.hlookat,
					vlookat: data.view.vlookat
				});
				$rootScope.$broadcast("viewChangeSuccess", ArcticData.current);
				$timeout(function () {
					$rootScope.$apply();
				},1);
			}
		});

		$rootScope.$on("krpanoready", function (event, data) {
			// console.log("krpanoready");
			deferredKrpanoApi.resolve(data.krpano);
			$timeout(function () {
				$rootScope.$apply();
			},1);
		});

		function parseCurrentXmlUrl (url) {
			var temp = ArcticData.current.xml.split("/"),
				params;
			if (temp.length < 2) {
				// console.log("Плохая ссылка на панораму", url);
			} else {
				params = {
					folder: temp[0],
					slide: parseInt(temp[1].split(".xml")[0])
				}
			}
			return params;
		}

		function getNextFolder (folder) {
			var nextId = ArcticData.all.order.indexOf(folder) + 1;
			if (nextId >= ArcticData.all.order.length) {
				nextId = 0;
			}
			return ArcticData.all.order[nextId];
		}

		function getPrevFolder (folder) {
			var prevId = ArcticData.all.order.indexOf(folder) - 1;
			if (prevId < 0) {
				prevId = ArcticData.all.order.length - 1;
			}
			return ArcticData.all.order[prevId];
		}

		function getNextPanoramaParams (params) {
			var nextParams = {
				folder: params.folder,
				slide: params.slide + 1
			};
			// А какие проблемы?
			// Исключение
			var exceptions = ArcticData.all.info[nextParams.folder].exceptions;
			if (exceptions) {
				if (exceptions.indexOf(nextParams.slide) > -1) {
					// console.log("Исключение")
					// Шлёпаем следующее
					return getNextPanoramaParams(nextParams);
				}
			}
			// Вышли за границу диапазона
			if (nextParams.slide > ArcticData.all.info[nextParams.folder].length) {
				// перейти в след. папку в 1
				// console.log("Вышли за границу диапазона");
				nextParams.folder = getNextFolder(nextParams.folder);
				nextParams.slide = 0;
				return getNextPanoramaParams(nextParams);
			}
			// Нет проблем
			return nextParams;
		}

		function getPrevPanoramaParams (params) {
			var prevParams = {
				folder: params.folder,
				slide: params.slide - 1
			}
			// А какие проблемы?
			// Исключение
			var exceptions = ArcticData.all.info[prevParams.folder].exceptions;
			if (exceptions) {
				if (exceptions.indexOf(prevParams.slide) > -1) {
					return getPrevPanoramaParams(prevParams);
				}
			}
			// Вышли за границу диапазона
			if (prevParams.slide < 1) {
				prevParams.folder = getPrevFolder(prevParams.folder);
				prevParams.slide = ArcticData.all.info[prevParams.folder].length + 1;
				return getPrevPanoramaParams(prevParams);
			}
			// Нет проблем
			return prevParams;
		}

		function nextPanorama () {
			var nextParams = getNextPanoramaParams(parseCurrentXmlUrl(ArcticData.current.xml)),
				nextPanoramaXml = nextParams.folder + "/" + ("000" + nextParams.slide).slice(-4) + ".xml";
			// console.log("next panorama", nextPanoramaXml);
			loadPanorama({
				xml: nextPanoramaXml
			});
			$rootScope.$broadcast("nextPanorama", nextPanoramaXml);
		}

		function prevPanorama () {
			var prevParams = getPrevPanoramaParams(parseCurrentXmlUrl(ArcticData.current.xml)),
				prevPanoramaXml = prevParams.folder + "/" + ("000" + prevParams.slide).slice(-4) + ".xml";
			// console.log("prev panorama", prevPanoramaXml);
			loadPanorama({
				xml: prevPanoramaXml
			});
			$rootScope.$broadcast("prevPanorama", prevPanoramaXml);
		}

		function loadPanorama (opts) {
			// console.log("load panorama", opts);
			opts.island = islandByXml(opts.xml);
			angular.extend(ArcticData.current, opts);
			delete ArcticData.current.autorotate;

			deferredKrpanoApi.promise.then(function (krpano) {
				if (opts.xml) {
					// console.log("load panorama opts hlookat", opts.hlookat);
					// console.log("load panorama Arctic hlookat", ArcticData.current.hlookat);
					var vars = "",
						viewtransition = ", MERGE";
					if (opts.autorotate) {
						vars += (vars ? "&amp;" : ", ") + "autorotate.enabled=true&amp;autorotate.speed=10";
					}
					if (safeReturnNumberVar(opts.hlookat) !== undefined) {
						vars += (vars ? "&amp;" : ", ") + "view.hlookat=" + safeReturnNumberVar(opts.hlookat);
					}
					if (safeReturnNumberVar(opts.vlookat) !== undefined) {
						vars += (vars ? "&amp;" : ", ") + "view.vlookat=" + safeReturnNumberVar(opts.vlookat);
					}
					if (!vars) {
						vars = ", null";
					}
					if (!opts.hlookat && !opts.vlookat) {
						viewtransition = ", KEEPVIEW";
					}
					// console.log(vars);
					krpano.call("loadpano(" +
						"../" + opts.xml +
						vars +
						viewtransition +
						// (opts.autorotate ? ", autorotate.enabled=true&amp;autorotate.speed=10" : ", null") +
						// ", null" +
						// (opts.hlookat ? ", view.hlookat=" + parseInt(opts.hlookat) : ",")
						// ", view.hlookat=" + (opts.hlookat ? opts.hlookat + ArcticData.current.hlookat) +
						// (opts.autorotate ? "&amp;autorotate.enabled=true&amp;autorotate.speed=10" : "") +
						", BLEND(0.5));");

					$rootScope.$broadcast("panoramaChangeSuccess", angular.copy(opts));
				}
				if (opts.hlookat || opts.vlookat) {
					// safeSetNumberVar("view.hlookat", opts.hlookat, krpano);
					// safeSetNumberVar("view.vlookat", opts.vlookat, krpano);
					$rootScope.$broadcast("viewChangeSuccess", angular.copy(opts));
				}
			});
		}

		function insertPanorama (opts) {
			var UA = navigator.userAgent;

			Embedpano({
				swf: opts.swf,
				xml: TourPath + opts.xml,
				target: opts.id,
				html5: 'prefer', //(UA.match(/iPad/g) || UA.match(/iPhone/g) || UA.match(/Android/g) ? "prefer" : "auto"),
				vars: opts.vars,
				bgcolor: "#115385",
				wmode: "opaque",
				// basepath: (opts.path ? opts.path : ""),
				passQueryParameters: true,
				onready: onkrpanoready
			});
			ArcticData.current.island = islandByXml(opts.xml);
			$rootScope.$broadcast("panoramaChangeSuccess", ArcticData.current);
			$rootScope.$broadcast("viewChangeSuccess", ArcticData.current);
		}

		function islandByXml (xml) {
			if (typeof xml != "string") {
				// console.log("Нужна строка адреса XML");
				return;
			}
			var foundIsland;
			for (var folder in ArcticData.folders) {
				if (ArcticData.folders.hasOwnProperty(folder)) {
					if (xml.match(folder)) {
						foundIsland = ArcticData.folders[folder];
						break;
					}
				}
			}
			return foundIsland;
		}

		function safeSetNumberVar (varname, varvalue, krpano) {
			if (krpano) {
				if (Number(varvalue) === 0 || Boolean(Number(varvalue))) {
					krpano.set(varname, Number(varvalue));
				}
			} else {
				deferredKrpanoApi.promise.then(function (pano) {
					safeSetVar(varname, varvalue, pano);
				});
			}
		}

		function safeReturnNumberVar (varvalue) {
			if (Number(varvalue) === 0 || Boolean(Number(varvalue))) {
				return Number(varvalue);
			}
		}

		return {
			insert: insertPanorama,
			api: deferredKrpanoApi.promise,
			next: nextPanorama,
			prev: prevPanorama,
			load: loadPanorama,
			setNumberVar: safeSetNumberVar
		}
	}])

	.directive("arcticPanorama", ["Panorama", "ArcticData", "$rootScope", "$location", function (Panorama, ArcticData, $rootScope, $location) {

		return {
			restrict: "EA",
			link: function ($scope, $element, $attrs) {

				// Инициализация панорамы
				// Берем данные либо из ссылки, либо из html
				angular.extend(ArcticData.current, {
					xml: ($location.search().xml ? $location.search().xml : ArcticData.current.xml),
					hlookat: ($location.search().hlookat ? $location.search().hlookat : ArcticData.current.hlookat),
					vlookat: ($location.search().vlookat ? $location.search().vlookat : ArcticData.current.vlookat)
				});
				// console.log(ArcticData.current);
				Panorama.insert({
					swf: $element.attr("data-swf"),
					id: $element.attr("id"),
					path: $element.attr("data-path"),
					xml: ArcticData.current.xml,
					vars: {
						"view.hlookat": ArcticData.current.hlookat,
						"view.vlookat": ArcticData.current.vlookat,
					}
				});

				// Для удобства сохраняем ссылку на действия панорамы
				$rootScope.panorama = {
					next: Panorama.next,
					prev: Panorama.prev,
					load: Panorama.load
				};

				// Когда готова панорама и её апи
				// можно что-то с ней сделать
				Panorama.api.then(function (krpano) {
					// console.log("krpano", krpano);
				});

			}
		}
	}]);