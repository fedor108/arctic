var gulp = require("gulp"),
	concat = require("gulp-concat"),
	uglify = require("gulp-uglify"),
	sourcemaps = require("gulp-sourcemaps"),
	// minifyCss = require("gulp-minify-css"),
	uglifycss = require('gulp-uglifycss'),
	autoprefixer = require("gulp-autoprefixer");

// App js
gulp.task("js", function () {
	return gulp.src([
			"js/source/*.js",
			"!js/source/*.test.js",
			"!js/source/skip*.js"
		])
		.pipe(sourcemaps.init())
		.pipe(concat("app.min.js"))
		.pipe(uglify({
			mangle: false
		}))
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest("js/"));
});

// Test js
gulp.task("tests", function () {
	return gulp.src([
			"!js/source/skip*.js",
			"js/source/*.test.js"
		])
		.pipe(concat("tests.min.js"))
		// .pipe(uglify({
		// 	mangle: false
		// }))
		.pipe(gulp.dest("js/"));
});

// App css
gulp.task("css", function () {
	return gulp.src([
		"css/source/*.css",
		"!css/source/skip*.css"
	])
	.pipe(concat("app.min.css"))
	.pipe(autoprefixer({
		browsers: ["last 4 versions"],
		cascade: false
	}))
	.pipe(uglifycss())
	.pipe(gulp.dest("css/"));
});

// Watch
gulp.task("watch", function () {
	gulp.watch("js/source/*.js", ["js"]);
	gulp.watch("js/source/*.test.js", ["tests"]);
	gulp.watch("css/source/*.css", ["css"]);
});

// Watch tests
gulp.task("watch-tests", function () {
});

// Manual lib js
gulp.task("lib-js", function () {
	return gulp.src([
			"bower_components/jquery/dist/jquery.min.js",
			"bower_components/autosize/dist/autosize.min.js",
			"bower_components/angular/angular.min.js",
			"bower_components/angular-animate/angular-animate.min.js",
			"bower_components/angular-ui-router/release/angular-ui-router.min.js"
		])
		.pipe(concat("lib.min.js"))
		.pipe(gulp.dest("js/"))
});

// Manual dev js
gulp.task("dev-js", function() {
	return gulp.src([
			"bower_components/jasmine-core/lib/jasmine-core/jasmine.js",
			"bower_components/jasmine-core/lib/jasmine-core/jasmine-html.js",
			"bower_components/jasmine-core/lib/jasmine-core/boot.js",
			"bower_components/testwood/testwood.js",
		])
		.pipe(concat("dev.min.js"))
		.pipe(gulp.dest("js/"));
});

// Manual lib css
// gulp.task("lib-css", function() {
// 	return gulp.src([
// 			""
// 		])
// 		.pipe(concat("lib.min.css"))
// 		.pipe(gulp.dest("css/"));
// });

// Manual dev css
gulp.task("dev-css", function() {
	return gulp.src([
			"bower_components/jasmine-core/lib/jasmine-core/jasmine.css",
			"bower_components/testwood/testwood.css"
		])
		.pipe(concat("dev.min.css"))
		.pipe(gulp.dest("css/"));
});


/*	Command-line API
 */

// "$ gulp"
// gulp.task("default", ["webserver", "watch"]);
gulp.task("default", ["watch"]);

// "$ gulp manual"
gulp.task("manual", ["dev-js", "dev-css", "lib-js"]);